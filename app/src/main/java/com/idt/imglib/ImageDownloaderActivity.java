package com.idt.imglib;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.io.File;

public class ImageDownloaderActivity extends AppCompatActivity {

    private static final String TAG = ImageDownloaderActivity.class.getSimpleName();

    private ImageView mImage;
    private String mLocation;
    private EditText mUrl;
    private ProgressBar mProgressBar;

    /**
     * Broadcast Receiver to handle image downloaded event (Ideally would have preferred to use LocalBroadcastManager)
     */
    private BroadcastReceiver mImageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mLocation = intent.getExtras().getString(ImageDownloaderService.LOCATION);
            setImage();
        }
    };

    /**
     * Lifecycle Methods
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_downloader);

        Button downloadButton = (Button) findViewById(R.id.download);
        mImage = (ImageView) findViewById(R.id.image);
        mUrl = (EditText) findViewById(R.id.url);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);

        if (savedInstanceState != null && savedInstanceState.containsKey("Location")) {
            mLocation = savedInstanceState.getString("Location");
            setImage();
        }

        downloadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStoragePermissionGranted()) {
                    startImageDownloaderService();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ImageDownloaderService.RESULT);
        registerReceiver(mImageReceiver, intentFilter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(mImageReceiver);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (mLocation != null) {
            outState.putString("Location", mLocation);
        }
    }

    /**
     * Function to start Intent Service to download image
     */
    private void startImageDownloaderService() {
        Intent intent = new Intent(getApplicationContext(), ImageDownloaderService.class);
        intent.putExtra(ImageDownloaderService.URL_EXTRA, mUrl.getText().toString());
        startService(intent);

        mImage.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.VISIBLE);
    }

    /**
     * Function to update the ImageView to show the downloaded image
     */
    private void setImage() {
        if (TextUtils.isEmpty(mLocation)) {
            Log.d(TAG, "Failed to download image");
        }

        File imageFile = new File(mLocation);
        if (!imageFile.exists()) {
            Log.d(TAG, "Unable to Download file :-(");
            return;
        }

        Bitmap b = Utils.decodeSampledBitmapFromResource(mLocation, 700, 700);

        Matrix matrix = new Matrix();
        matrix.postRotate(180);
        Bitmap rotatedBitmap = Bitmap.createBitmap(b, 0, 0, 700, 700, matrix, true);

        mImage.setImageBitmap(rotatedBitmap);
        mImage.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.GONE);
    }

    /**
     * Function to check if Storage permission has been granted
     */
    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "Permission is granted");
                return true;
            } else {

                Log.d(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.d(TAG, "Permission is granted");
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "Permission: " + permissions[0] + " was " + grantResults[0]);
            startImageDownloaderService();
        }
    }
}