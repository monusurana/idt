package com.idt.imglib;

import android.app.IntentService;
import android.content.Intent;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by msurana on 5/21/2016.
 */
public class ImageDownloaderService extends IntentService {

    private static final String TAG = ImageDownloaderService.class.getSimpleName();

    public static final String RESULT = "result";
    public static final String URL_EXTRA = "url";
    public static final String LOCATION = "location";
    private static final String CACHE_DIR = "/ImageCache";

    private File mCacheDir;

    public ImageDownloaderService() {
        super(TAG);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        String tmpLocation = Environment.getExternalStorageDirectory().toString() + CACHE_DIR;

        mCacheDir = new File(tmpLocation);
        if(!mCacheDir.exists()){
            mCacheDir.mkdirs();
        }

    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String remoteUrl = intent.getExtras().getString(URL_EXTRA);
        String filename = remoteUrl.substring(remoteUrl.lastIndexOf(File.separator) + 1);
        File tmp = new File(mCacheDir.getPath() + File.separator + filename);

        String location;

        Log.d(TAG, "Downloading from URL: " + remoteUrl);
        Log.d(TAG, "Saved File Location: " + tmp.getAbsolutePath());

        //If file already exists, no need to download again
        if (tmp.exists()) {
            Log.d(TAG, "Exists already");

            location = tmp.getAbsolutePath();
            notifyFinished(location);
            stopSelf();
            return;
        }

        try {
            URL url = new URL(remoteUrl);
            HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();

            if (httpCon.getResponseCode() != 200) {
                throw new Exception("Failed to connect");
            }

            writeToFile(httpCon.getInputStream(), tmp);
            location = tmp.getAbsolutePath();

            notifyFinished(location);
        } catch (Exception e) {
            Log.e(TAG, "Failed!", e);
        }
    }

    /**
     * Function to write date from inputStream to File
     */
    private void writeToFile(InputStream is, File tmp) throws IOException {
       FileOutputStream fos = new FileOutputStream(tmp);

        int read;
        byte[] bytes = new byte[1024];

        while ((read = is.read(bytes)) != -1) {
            fos.write(bytes, 0, read);
        }

        fos.flush();
        fos.close();
        is.close();
    }

    /**
     * Send a broadcast event to notify Activity that image has been downloaded
     */
    private void notifyFinished(String location){
        Intent i = new Intent(RESULT);
        i.putExtra(LOCATION, location);
        ImageDownloaderService.this.sendBroadcast(i);
    }
}
